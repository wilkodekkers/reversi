﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using Google.Authenticator;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using ReversiApp.DAL;
using ReversiApp.Models;

namespace ReversiApp.Controllers
{
    public class AuthController : Controller
    {
        private readonly ReversiContext _context;

        public AuthController(ReversiContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return RedirectToAction("Login");
        }

        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Spel");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Id,Naam,Wachtwoord,Email")] Speler speler)
        {
            var httpClient = new HttpClient();
            var token = Request.Form["g-recaptcha-response"];
            var res = await httpClient.GetAsync($"https://www.google.com/recaptcha/api/siteverify?secret=6LdHwgcUAAAAAAf5E399jMeZF5yepc8l1pnYsSSF&response={token}");
            var jsonResponse = res.Content.ReadAsStringAsync().Result;
            dynamic jsonData = JObject.Parse(jsonResponse);

            if (jsonData.success != "true")
            {
                ViewData["Error"] = "Captcha empty!";
                return View("Login", speler);
            }

            var user = _context.Spelers.FirstOrDefault(i => i.Email == speler.Email);

            if (user == null)
            {
                ViewData["Error"] = "User not found!";
                return View("Login", speler);
            }

            if (!Crypto.VerifyHashedPassword(user.Wachtwoord, speler.Wachtwoord))
            {
                ViewData["Error"] = "The credentials does not match!";
                return View("Login", speler);
            }

            if (user.TwoFactorAuth)
            {
                TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
                string userUniqueKey = user.Email + user.Token;
                var setupInfo = twoFactorAuthenticator.GenerateSetupCode("Reversi - wilkodekers.hbo-ict.org", speler.Email, Encoding.ASCII.GetBytes(userUniqueKey), 10);

                HttpContext.Session.SetString("UserEmail", speler.Email);
                HttpContext.Session.SetString("UserUniqueKey", userUniqueKey);

                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.SetupCode = setupInfo.ManualEntryKey;

                return View("Verify");
            }

            ClaimsIdentity identity = new ClaimsIdentity(GetUserRoleClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
            ClaimsPrincipal principal = new ClaimsPrincipal(identity);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

            return RedirectToAction("Index", "Spel");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> TwoFactorAuthenticate(string codeDigit)
        {
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            string UserUniqueKey = HttpContext.Session.GetString("UserUniqueKey");

            if (TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, codeDigit))
            {
                var user = _context.Spelers.FirstOrDefault(i => i.Email == HttpContext.Session.GetString("UserEmail"));

                if (user == null)
                {
                    return RedirectToAction("Login");
                }

                ClaimsIdentity identity = new ClaimsIdentity(GetUserRoleClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
                ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                return RedirectToAction("Index", "Spel");
            }

            return RedirectToAction("Login");
        }

        public IActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Spel");
            }

            return Redirect("/Speler/Create");
        }

        private IEnumerable<Claim> GetUserRoleClaims(Speler speler)
        {
            return new List<Claim>()
            {
                new Claim(ClaimTypes.NameIdentifier, speler.Id.ToString()),
                new Claim(ClaimTypes.Name, speler.Naam),
                new Claim(ClaimTypes.Email, speler.Email),
                new Claim(ClaimTypes.Role, speler.Rol.ToString()),
                new Claim(ClaimTypes.Authentication, speler.Token),
            };
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }
    }
}