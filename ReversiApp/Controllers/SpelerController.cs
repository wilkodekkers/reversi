using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using Google.Authenticator;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ReversiApp.DAL;
using ReversiApp.Models;

namespace ReversiApp.Controllers
{
    public class SpelerController : Controller
    {
        private readonly ReversiContext _context;

        public SpelerController(ReversiContext context)
        {
            _context = context;
        }

        // GET: Speler/Details
        [Authorize(Roles = "Speler")]
        public async Task<IActionResult> Details()
        {
            var speler = await _context.Spelers
                .Include(s => s.Spel)
                .FirstOrDefaultAsync(m => m.Id == int.Parse(User.Claims.ToArray()[0].Value));
            if (speler == null)
            {
                return NotFound();
            }

            if (speler.TwoFactorAuth)
            {
                TwoFactorAuthenticator twoFactorAuthenticator = new TwoFactorAuthenticator();
                string userUniqueKey = speler.Email + speler.Token;
                var setupInfo = twoFactorAuthenticator.GenerateSetupCode("Reversi - wilkodekkers.hbo-ict.org", speler.Email, Encoding.ASCII.GetBytes(userUniqueKey), 10);

                HttpContext.Session.SetString("UserEmail", speler.Email);
                HttpContext.Session.SetString("UserUniqueKey", userUniqueKey);

                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.SetupCode = setupInfo.ManualEntryKey;
            }

            ViewBag.TotalGames = _context.Spellen.Where(s => s.ID == speler.Id).Count();

            return View(speler);
        }

        // GET: Speler/Create
        public IActionResult Create()
        {
            ViewData["SpelId"] = new SelectList(_context.Spellen, "ID", "SerializeBord");
            return View();
        }

        // POST: Speler/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naam,Wachtwoord,Email")] Speler speler)
        {
            if (ModelState.IsValid)
            {
                speler.Wachtwoord = Crypto.HashPassword(speler.Wachtwoord);
                _context.Add(speler);
                await _context.SaveChangesAsync();
                return Redirect("/Auth/Login");
            }
            else
            {
                return Redirect("/Speler/Create");
            }
        }

        // GET: Speler/Edit/5
        [Authorize(Roles = "Speler")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var speler = await _context.Spelers.FindAsync(id);
            if (speler == null)
            {
                return NotFound();
            }
            ViewData["SpelId"] = new SelectList(_context.Spellen, "ID", "SerializeBord", speler.SpelId);
            return View(speler);
        }

        // POST: Speler/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Speler")]
        public async Task<IActionResult> Edit([Bind("Id,Naam,Email,TwoFactorAuth,Wachtwoord")] Speler speler)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(speler);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpelerExists(speler.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Redirect("/Speler/Details");
            }
            ViewData["SpelId"] = new SelectList(_context.Spellen, "ID", "SerializeBord", speler.SpelId);
            return Redirect("/Speler/Details");
        }

        private bool SpelerExists(int id)
        {
            return _context.Spelers.Any(e => e.Id == id);
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword([Bind("Id,Naam,Email,TwoFactorAuth,Wachtwoord")] Speler speler, string codeDigit)
        {
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            Speler user = _context.Spelers.FirstOrDefault(i => i.Email == speler.Email);

            if (user == null)
            {
                ViewBag.Error = "Invalid email";
                return View(speler);
            }

            string secret = user.Email + user.Token;

            if (!TwoFacAuth.ValidateTwoFactorPIN(secret, codeDigit))
            {
                ViewBag.Error = "Invalid code";
                return View(user);
            }

            user.Wachtwoord = Crypto.HashPassword(speler.Wachtwoord);
            _context.Spelers.Update(user);
            await _context.SaveChangesAsync();

            return Redirect("/Auth/Login");
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(string password1, string password2)
        {
            if (password1.Equals(password2))
            {
                int playerId = int.Parse(User.Claims.ToArray()[0].Value);

                var player = _context.Spelers.Find(playerId);
                player.Wachtwoord = Crypto.HashPassword(password1);
                _context.Update(player);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Spel");
            }

            return View();
        }
    }
}
