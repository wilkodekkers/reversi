using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ReversiApp.DAL;
using ReversiApp.Models;

namespace ReversiApp.Controllers
{
    [Authorize(Roles = "Speler")]
    public class SpelController : Controller
    {
        private readonly ReversiContext _context;

        public SpelController(ReversiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var speler = await _context.Spelers
                .Include(s => s.Spel)
                .FirstOrDefaultAsync(m => m.Id == int.Parse(User.Claims.ToArray()[0].Value));

            if (speler.Spel == null)
            {
                var games = await _context.Spellen
                    .Include(g => g.Spelers)
                    .Where(g => g.Spelers.Count < 2)
                    .Where(g => !g.Spelers.Contains(speler))
                    .ToListAsync();

                if (games == null)
                {
                    return View("List", new List<Spel>());
                }

                games = games.Where(g => !g.Afgelopen()).ToList();

                return View("List", games);
            }

            return View("Index", speler);
        }

        // GET: Spel/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Spel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Omschrijving,Token,SerializeBord,AandeBeurt")] Spel spel)
        {
            if (ModelState.IsValid)
            {
                var userId = int.Parse(User.Claims.ToArray()[0].Value);
                var user = await _context.Spelers.FindAsync(userId);

                spel.Spelers = new List<Speler>();

                spel.Spelers.Add(user);
                _context.Add(spel);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(spel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> JoinGame([Bind("ID")] Spel spel)
        {
            var foundGame = await _context.Spellen.FindAsync(spel.ID);
            var user = await _context.Spelers.FindAsync(int.Parse(User.Claims.ToArray()[0].Value));

            if (foundGame == null)
            {
                return RedirectToAction("Index");
            }

            user.Spel = foundGame;
            user.SpelId = foundGame.ID;

            _context.Update(user);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
