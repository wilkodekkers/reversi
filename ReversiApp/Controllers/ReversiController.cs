using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ReversiApp.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ReversiApp.DAL;
using Microsoft.EntityFrameworkCore;

namespace ReversiApp.Controllers
{
    [Route("api/Spel")]
    public class ReversiController : Controller
    {
        private static readonly List<Spel> Games = new List<Spel>();
        private readonly ReversiContext _context;

        public ReversiController(ReversiContext context)
        {
            _context = context;
        }

        // GET: /api/Spel/<token>
        [HttpGet("{token}")]
        public ActionResult GetGame(string token)
        {
            var foundGame = _context.Spellen.FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            return Ok(JsonConvert.SerializeObject(foundGame));
        }

        // GET: /api/Spel/<token>/Beurt
        [HttpGet("{token}/Beurt")]
        public ActionResult GetTurn(string token)
        {
            var foundGame = _context.Spellen.FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            return Ok(JsonConvert.SerializeObject(foundGame.AandeBeurt));
        }

        // GET: /api/Spel/<token>/winning
        [HttpGet("{token}/winning")]
        public ActionResult GetWinning(string token)
        {
            var foundGame = _context.Spellen.FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            return Ok(foundGame.OverwegendeKleur());
        }

        [HttpGet("{token}/players")]
        public IActionResult GetPlayers(string token)
        {
            var foundGame = _context.Spellen
                .Include(game => game.Spelers)
                .FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            List<string> playerNames = new List<string>();

            for (int i = 0; i < foundGame.Spelers.Count; i++)
            {
                playerNames.Add(foundGame.Spelers[i].Naam);
            }

            return Ok(playerNames);
        }

        // GET: /api/Spel/<token>/over
        [HttpGet("{token}/over")]
        public async Task<IActionResult> GetOver(string token)
        {
            var foundGame = _context.Spellen
                .Include(game => game.Spelers)
                .FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            if (foundGame.Afgelopen())
            {
                return Ok(true);
            }
            else
            {
                return Ok(false);
            }
        }

        [HttpPost("{token}/{playerToken}/leave")]
        public async Task<IActionResult> LeaveGame(string token, string playerToken)
        {
            var foundGame = _context.Spellen
               .Include(game => game.Spelers)
               .FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not find the requested game");
            }

            if (playerToken == null)
            {
                return NotFound("Could not find the requested player");
            }

            var player = foundGame.Spelers.Where(x => x.Token.Equals(playerToken)).FirstOrDefault();

            if (player == null)
            {
                return NotFound("Player not in the game");
            }

            player.SpelId = null;
            player.Spel = null;

            _context.Update(player);
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet("{token}/whoAmI/{playerToken}")]
        public IActionResult GetWhoAmI(string token, string playerToken)
        {
            var foundGame = _context.Spellen
                .Include(game => game.Spelers)
                .FirstOrDefault(game => game.Token == token);

            if (foundGame == null)
            {
                return NotFound("Could not find the requested game");
            }

            if (playerToken == null)
            {
                return NotFound("Could not find the requested player");
            }

            if (foundGame.Spelers[0].Token.Equals(playerToken))
            {
                return Ok(1);
            }
            else if (foundGame.Spelers[1].Token.Equals(playerToken))
            {
                return Ok(2);
            }

            return NotFound("Player not found");
        }

        // POST: /api/Spel/<token>/place
        [HttpPost("{gameToken}/place")]
        [Produces("application/json")]
        public async Task<ActionResult> PlaceTile(string gameToken, string playerToken, string x, string y, bool passTurn)
        {
            if (string.IsNullOrEmpty(gameToken)) return NotFound("Game Token can't be empty");
            if (string.IsNullOrEmpty(playerToken)) return NotFound("Player Token can't be empty");
            if (string.IsNullOrEmpty(x)) return NotFound("Player Token can't be empty");
            if (string.IsNullOrEmpty(y)) return NotFound("Player Token can't be empty");

            var foundGame = _context.Spellen
                .Include(game => game.Spelers)
                .FirstOrDefault(game => game.Token == gameToken);

            if (foundGame == null)
            {
                return NotFound("Could not found the requested game");
            }

            if (foundGame.Afgelopen())
            {
                return NotFound("This game is over");
            }

            if (foundGame.Spelers.Count < 2)
            {
                return NotFound("Waiting for another player to join");
            }

            var inputPlayer = foundGame.Spelers.Single(s => s.Token == playerToken);

            if (inputPlayer == null)
            {
                return NotFound("Player not in this game");
            }

            if (inputPlayer.Token != foundGame.Spelers.ToArray()[(int)foundGame.AandeBeurt - 1].Token)
            {
                return NotFound("It is not your turn");
            }

            if (passTurn)
            {
                if (foundGame.Pas())
                {
                    _context.Update(foundGame);
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NotFound("Could not pass");
                }
            }

            if (foundGame.DoeZet(int.Parse(x), int.Parse(y)))
            {
                _context.Update(foundGame);
                await _context.SaveChangesAsync();
                return Ok();
            }
            else
            {
                return NotFound("Could not place the tile");
            }
        }
    }
}