using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace ReversiApp.Models
{
    public class Spel : ISpel
    {
        public int ID { get; set; }
        public string Omschrijving { get; set; }
        public string Token { get; set; }
        public string SerializeBord
        {
            get => JsonConvert.SerializeObject(Bord);
            set => Bord = JsonConvert.DeserializeObject<Kleur[,]>(value);
        }
        [NotMapped]
        public Kleur[,] Bord { get; set; } = new Kleur[8, 8];
        public Kleur AandeBeurt { get; set; } = Kleur.Zwart;
        private List<(int, int)> PossibleDirection { get; set; } = new List<(int, int)>();
        public List<Speler> Spelers { get; set; }

        public Spel()
        {
            // generate new unique token
            var g = Guid.NewGuid();
            var GuidString = g.ToString().Replace("&", "");
            

            Token = GuidString;

            // initialize middle of bord
            Bord[3, 3] = Kleur.Wit;
            Bord[4, 3] = Kleur.Zwart;
            Bord[3, 4] = Kleur.Zwart;
            Bord[4, 4] = Kleur.Wit;

            SerializeBord = JsonConvert.SerializeObject(Bord);
        }
        public bool Pas()
        {
            // check if there is a play possible
            for (var row = 0; row < Bord.GetLength(0); row++)
            {
                for (var col = 0; col < Bord.GetLength(1); col++)
                {
                    // if there is a play possible, then you may not pass the turn
                    var canPlay = CheckPlay(row, col, AandeBeurt);
                    if (canPlay) return false;
                }
            }

            // switch color
            AandeBeurt = OppositeColor(AandeBeurt);
            return true;
        }

        public bool Afgelopen()
        {
            // for each field, check if a move is possible
            for (var row = 0; row < Bord.GetLength(0); row++)
            {
                for (var col = 0; col < Bord.GetLength(1); col++)
                {
                    // check if white can play
                    var whiteCanPlay = CheckPlay(row, col, Kleur.Zwart);
                    // check if black can play
                    var blackCanPlay = CheckPlay(row, col, Kleur.Wit);
                    // if white or black can still play, return false
                    if (whiteCanPlay || blackCanPlay) return false;
                }
            }

            // if there is no move found, the game is over
            return true;
        }

        public Kleur OverwegendeKleur()
        {
            // initialize variables
            var white = 0;
            var black = 0;

            // check colors on the board
            foreach (var color in Bord)
            {
                switch (color)
                {
                    case Kleur.Wit:
                        white++;
                        break;
                    case Kleur.Zwart:
                        black++;
                        break;
                    case Kleur.Geen:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // return color
            return white == black ? Kleur.Geen : white < black ? Kleur.Zwart : Kleur.Wit;
        }

        public bool ZetMogelijk(int rijZet, int kolomZet)
        {
            return CheckPlay(rijZet, kolomZet, AandeBeurt);
        }

        public bool DoeZet(int rowSet, int colSet)
        {
            if (!ZetMogelijk(rowSet, colSet)) return false;

            // place set
            Bord[rowSet, colSet] = AandeBeurt;

            // turn tiles
            foreach (var (r, c) in PossibleDirection)
            {
                Bord[r, c] = AandeBeurt;
            }

            // switch turn
            AandeBeurt = OppositeColor(AandeBeurt);

            return true;
        }

        public static Kleur OppositeColor(Kleur color)
        {
            // return opposite color
            return color switch
            {
                Kleur.Geen => Kleur.Geen,
                Kleur.Wit => Kleur.Zwart,
                Kleur.Zwart => Kleur.Wit,
                _ => Kleur.Geen
            };
        }

        private bool CheckPlay(int row, int col, Kleur color)
        {
            // initialize variables
            var turnLocations = new List<(int, int)>();

            // check possible directions to go
            var possibleDirections = GetPossibleDirections(row, col, color);

            // when no directions are possible return false
            if (possibleDirections.Count == 0) return false;

            // loop through all possibilities
            foreach (var direction in possibleDirections)
            {
                var options = CheckDirectionForPlays(direction, (row, col));
                foreach (var (item1, item2) in options)
                {
                    turnLocations.Add((item1, item2));
                }
            }

            PossibleDirection = turnLocations;

            return PossibleDirection.Count > 0;
        }

        private List<(int, int)> CheckDirectionForPlays(Direction direction, (int, int) place)
        {
            var (row, col) = place;
            var temp = new List<(int, int)>();
            var foundSelf = false;
            var rowCounter = row;
            var colCounter = col;

            switch (direction)
            {
                case Direction.North:
                    rowCounter--;
                    while (rowCounter > 0)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter--;
                    }
                    break;
                case Direction.NorthEast:
                    rowCounter--;
                    colCounter++;
                    while (rowCounter >= 0 && colCounter < 8)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter--;
                        colCounter++;
                    }
                    break;
                case Direction.East:
                    colCounter++;
                    while (colCounter < 8)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        colCounter++;
                    }
                    break;
                case Direction.SouthEast:
                    rowCounter++;
                    colCounter++;
                    while (rowCounter < 8 && colCounter < 8)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter++;
                        colCounter++;
                    }
                    break;
                case Direction.South:
                    rowCounter++;
                    while (rowCounter < 8)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter++;
                    }
                    break;
                case Direction.SouthWest:
                    rowCounter++;
                    colCounter--;
                    while (rowCounter < 8 && colCounter > 0)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter++;
                        colCounter--;
                    }
                    break;
                case Direction.West:
                    colCounter--;
                    while (colCounter >= 0)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        colCounter--;
                    }
                    break;
                case Direction.NorthWest:
                    rowCounter--;
                    colCounter--;
                    while (rowCounter - 1 > 0 && colCounter - 1 > 0)
                    {
                        if (Bord[rowCounter, colCounter] == OppositeColor(AandeBeurt))
                        {
                            temp.Add((rowCounter, colCounter));
                        }
                        else if (Bord[rowCounter, colCounter] == AandeBeurt)
                        {
                            foundSelf = true;
                            break;
                        }
                        else
                            break;
                        rowCounter--;
                        colCounter--;
                    }
                    break;
                case Direction.Self:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            if (foundSelf)
                return temp;
            return new List<(int, int)>();
        }

        private List<Direction> GetPossibleDirections(int row, int col, Kleur color)
        {
            var directions = new List<Direction>();

            // check out of bounds
            if (row < 0 || row > 7 || col < 0 || col > 7)
                return directions;

            // check if empty
            if (Bord[row, col] != Kleur.Geen)
                return directions;

            // north
            if (row > 0)
                if (Bord[row - 1, col] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.North);

            // north east
            if (row > 0 && col < 7)
                if (Bord[row - 1, col + 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.NorthEast);

            // east
            if (col < 7)
                if (Bord[row, col + 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.East);

            // south east
            if (col < 7 && row < 7)
                if (Bord[row + 1, col + 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.SouthEast);

            // south
            if (row < 7)
                if (Bord[row + 1, col] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.South);

            // south west
            if (row < 7 && col > 0)
                if (Bord[row + 1, col - 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.SouthWest);

            // west
            if (col > 0)
                if (Bord[row, col - 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.West);

            // north west
            if (col > 0 && row > 0)
                if (Bord[row - 1, col - 1] == OppositeColor(AandeBeurt))
                    directions.Add(Direction.NorthWest);

            return directions;
        }
    }
}