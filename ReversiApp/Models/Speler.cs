using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReversiApp.Models
{
    public class Speler
    {
        public int Id { get; set; }
        [DisplayName("Gebruikersnaam")]
        [Required(ErrorMessage = "Er is geen gebruikersnaam ingevuld")]
        public string Naam { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Er is geen wachtwoord ingevuld!")]
        [RegularExpression(
            "^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&+=!])([a-zA-Z0-9]).*$",
            ErrorMessage = "Minimaal 1 normale letter, 1 cijfer, 1 speciaal symbool!"
        )]
        public string Wachtwoord { get; set; }
        [Required(ErrorMessage = "Er is geen emailadres ingevuld!")]
        [EmailAddress(ErrorMessage = "Er is geen geldige emailadres ingevuld!")]
        public string Email { get; set; }
        public string Token { get; set; } = Guid.NewGuid().ToString();
        public Kleur? Kleur { get; set; }
        public Rol Rol { get; set; } = Rol.Speler;
        public bool TwoFactorAuth { get; set; } = false;

        [ForeignKey("Spel")]
        public int? SpelId { get; set; }
        public Spel Spel { get; set; }
    }
}