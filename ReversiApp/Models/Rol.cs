﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiApp.Models
{
    public enum Rol
    {
        Speler, Moderator, Administrator
    }
}
