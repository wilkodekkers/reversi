﻿using System;

namespace ReversiApp.Models
{
    public enum Kleur
    {
        Geen = 0,
        Wit = 1,
        Zwart = 2
    };
}
