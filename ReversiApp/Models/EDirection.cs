namespace ReversiApp.Models
{
    public enum Direction
    {
        Self, North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest
    }
}