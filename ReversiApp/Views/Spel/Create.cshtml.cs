using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using ReversiApp.DAL;
using ReversiApp.Models;

namespace ReversiApp.Views
{
    public class CreateModel : PageModel
    {
        private readonly ReversiApp.DAL.ReversiContext _context;

        public CreateModel(ReversiApp.DAL.ReversiContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Spel Spel { get; set; }

        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Spellen.Add(Spel);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
