using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ReversiApp.DAL;
using ReversiApp.Models;

namespace ReversiApp.Views
{
    public class IndexModel : PageModel
    {
        private readonly ReversiApp.DAL.ReversiContext _context;

        public IndexModel(ReversiApp.DAL.ReversiContext context)
        {
            _context = context;
        }

        public IList<Spel> Spel { get;set; }

        public async Task OnGetAsync()
        {
            Spel = await _context.Spellen.ToListAsync();
        }
    }
}
