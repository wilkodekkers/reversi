using Microsoft.EntityFrameworkCore;
using ReversiApp.Models;

namespace ReversiApp.DAL
{
    public class ReversiContext : DbContext
    {
        public ReversiContext(DbContextOptions<ReversiContext> options) : base(options)
        {
        }
        
        public DbSet<Spel> Spellen { get; set; }
        public DbSet<Speler> Spelers { get; set; }
    }
}