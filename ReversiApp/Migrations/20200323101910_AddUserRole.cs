﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReversiApp.Migrations
{
    public partial class AddUserRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spelers_Spellen_SpelId",
                table: "Spelers");

            migrationBuilder.AlterColumn<string>(
                name: "Wachtwoord",
                table: "Spelers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SpelId",
                table: "Spelers",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "Naam",
                table: "Spelers",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Kleur",
                table: "Spelers",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Spelers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Rol",
                table: "Spelers",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Spelers_Spellen_SpelId",
                table: "Spelers",
                column: "SpelId",
                principalTable: "Spellen",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Spelers_Spellen_SpelId",
                table: "Spelers");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Spelers");

            migrationBuilder.DropColumn(
                name: "Rol",
                table: "Spelers");

            migrationBuilder.AlterColumn<string>(
                name: "Wachtwoord",
                table: "Spelers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "SpelId",
                table: "Spelers",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Naam",
                table: "Spelers",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "Kleur",
                table: "Spelers",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Spelers_Spellen_SpelId",
                table: "Spelers",
                column: "SpelId",
                principalTable: "Spellen",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
